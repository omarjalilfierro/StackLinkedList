package app;



import linkedList.LinkedList;
import linkedList.StackEmptyExeption;
import linkedList.StackFullExeption;

public class App {
	
	public static void main(String[] args) throws StackFullExeption, StackEmptyExeption {
		LinkedList<String> names = new LinkedList<>();
		
		names.push("Primero");
		names.push("Segundo");
		names.push("Tercero");
		names.push("Cuarto");
		names.push("Quinto");
		System.out.println("Este es el peek: " + names.peek());
		names.pop();
		names.push("Pepino");
		System.out.println("Size: " + names.size());
		
		for (String string : names) {
			System.out.println("Iterator: " + string);
		}
		
		System.out.println("Search: " + names.Search("Primero").getValue());
	}	
}
