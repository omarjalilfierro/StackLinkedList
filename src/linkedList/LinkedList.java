package linkedList;

import java.util.Iterator;

import nodo.Nodo;
import linkedList.StackEmptyExeption;
import linkedList.StackFullExeption;

public class LinkedList<T> implements iStack<T>,Iterable<T>{

	private Nodo<T> sent = null;
	int top = -1;
	int count = 0;

	public LinkedList() {
		sent = new Nodo<T>();
		sent.setIndex(-1);
		top = 5;
	}
		
	public LinkedList(T value,int topp){
		this();
		Nodo<T> tmp = new Nodo<T>(value);
		tmp.setIndex(0);
		sent.setNext(tmp);
		top = topp;
	}
	
	
    public Nodo<T> Search(T temp){
    	return Search(sent,temp);
	}
    private Nodo<T> Search(Nodo<T> se,T comp){
			if (se.getNext()==null){
				return null;
			}
			if(se.getNext().getValue().equals(comp)){
					return se.getNext();
				}
			return Search(se.getNext(),comp);
					
	} 
	public void reIndex(){
		Nodo<T> temp = sent;
		int cont = 0;
		while(temp.getNext()!=null){
		temp = temp.getNext();
		temp.setIndex(cont++);
		}
	}
	public void print(){
		Nodo<T> temp = sent;
		while(temp.getNext()!=null){
			temp = temp.getNext();
			System.out.println(temp.getValue());
		}
	}
	public boolean isEmpty(){
	if(sent.getNext() != null)
		return false;
	
		return true;
	}
	public void clear(){
		 Nodo<T> temp = sent;
		 while (temp.getNext() != null){
			 Nodo<T> temp2 = temp.getNext();
			 temp.setNext(null);
			 temp = temp2;
		 }
		 count = 0;
		 System.gc();
	 }
	public Nodo<T> getLast(){
		 Nodo<T> temp = sent;	
			while(temp.getNext()!=null)
				temp = temp.getNext();
			return temp;
	 }
	public Nodo<T> remove(T bus){
			Nodo<T> temp = sent;
			
				while (temp.getNext()!=null){
					if(temp.getNext().getValue().equals(bus)){
						Nodo<T> fin =temp.getNext();
						temp.setNext(temp.getNext().getNext());
						//System.out.println(fin.getValue());
						return fin;
					}
					temp = temp.getNext();
				}
					return null;	
		}
	public int size(){
		 return count;
	 }
	public Nodo<T> getByIndex(int i){
		 Nodo<T> temp = sent;
		 int n=0;
		 while ( n != i ) {
			 if(temp.getNext() != null)
			      temp= temp.getNext();
			n++;
		 }
		 return temp;
	}
	 @Override
	public Iterator<T> iterator() {
			return new Iterator<T>(){
				int i = 0;
				@Override
				public boolean hasNext() {
						return getByIndex(i).getNext()!=null ? true : false ;
				}
				@Override
				public T next() {
						return (T) getByIndex(++i).getValue();
				}	
			};
	 }
	@Override
	public T pop() throws StackEmptyExeption {
		if (count > 0){
			Nodo<T> tmp = new Nodo<>(getLast().getValue());
			remove( getLast().getValue());
			count--;
			return tmp.getValue();
		}
		throw new StackEmptyExeption("Empty Stack");
	}
	@Override
	public void push(T d) throws StackFullExeption {
		if (isFull()) throw new StackFullExeption("Full Stack");
		Nodo<T> n = new Nodo<T>(d);
		Nodo<T> temp = sent;	
		while(temp.getNext()!=null)
			temp = temp.getNext();
		n.setIndex(temp.getIndex()+1);
		temp.setNext(n);	
		count++;
	}
	@Override
	public boolean isFull() {
		return (count == top);
	}
	@Override
	public T peek() throws StackEmptyExeption {
		if (count > 0){
			Nodo<T> tmp = new Nodo<>(getLast().getValue());
			return tmp.getValue();
		}
		throw new StackEmptyExeption("Empty Stack");
	}
	
	
	
	
	}
