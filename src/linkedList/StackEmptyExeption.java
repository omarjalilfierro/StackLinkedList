package linkedList;

public class StackEmptyExeption extends Exception {
 public StackEmptyExeption(){}
 public StackEmptyExeption(String causa){
	 super(causa);
 }
 public StackEmptyExeption(Throwable causa){
	 super(causa);
 }
}
