package linkedList;

import linkedList.StackEmptyExeption;
import linkedList.StackFullExeption;

public interface iStack<T> {
	
	public T pop() throws StackEmptyExeption;
	public void push(T d)throws StackFullExeption;
	public boolean isEmpty();
	public boolean isFull();
	public T peek() throws StackEmptyExeption;
	public int size();
	public void clear();
}
